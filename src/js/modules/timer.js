const timer = (id, deadline) => {

    const addZero = (num) => {
        return (num <= 9) ? `0${num}` : num
    }

    const getTimeRemaining = (endTime) => {
        const t = Date.parse(endTime) - Date.parse(new Date()),
            seconds = Math.floor((t / 1000) % 60),
            minutes = Math.floor((t / 1000 / 60) % 60),
            hours = Math.floor((t / 1000 / 60 / 60) % 24),
            days = Math.floor((t / 1000 / 60 / 60 / 24))
        return {
            'total': t,
            days,
            hours,
            minutes,
            seconds
        }
    }
    const setClock = (selector, endTime) => {
        const timer = document.querySelector(selector),
            nodeDays = timer.querySelector('#days'),
            nodeHours = timer.querySelector('#hours'),
            nodeMinutes = timer.querySelector('#minutes'),
            nodeSeconds = timer.querySelector('#seconds'),
            timeInterval = setInterval(updateClock, 1000);

        updateClock();

        function updateClock() {
            const {total , days, hours, minutes, seconds} = getTimeRemaining(endTime);

            nodeDays.textContent = addZero(days);
            nodeHours.textContent = addZero(hours);
            nodeMinutes.textContent = addZero(minutes);
            nodeSeconds.textContent = addZero(seconds);

            if (total <= 0) {
                nodeDays.textContent = '00';
                nodeHours.textContent = '00';
                nodeMinutes.textContent = '00';
                nodeSeconds.textContent = '00';

                clearInterval(timeInterval);
            }
        }
    };

    setClock(id, deadline);
};
export default timer;